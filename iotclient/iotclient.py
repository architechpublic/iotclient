from .client import Client
import yaml
from Crypto.PublicKey import RSA
import msgpack
from Crypto import Random
from Crypto.Cipher import AES, PKCS1_OAEP
import base64

BLOCK_SIZE=16
PASS_SIZE = 32

class IoTClient(object):

    def __init__(self, port, hostname):
        self.port = port
        self.hostname = hostname
        self.to_encrypt = [None, None, None, '0']
        self._pass_idx = 0
        self._iv_idx = 1
        self._msname_idx = 2
        self._socket_idx = 3
        self.set_socket_type("keep-alive")
        self.c = None

    def send(self, message, microservicename):
        if not isinstance(message, bytes):
            message = msgpack.packb(message)

        self._set_pass()
        self._set_iv()
        self._set_microservicename(microservicename)

        self.stamp = self.enc.encrypt(self._iot_message())


        self.letter =  self.stamp + self._encrypt(message)

        self._switch_socket()(self)


    def _switch_socket(self):
        def _raise():
            print("Key Not Found")
        print(self.socket_type)
        a = {
            '0' : lambda x : x._keep_alive(),
            '1' : lambda x : x._one_shot(),
            '2' : lambda x : x._full_duplex(),
            '3' : lambda x : x._keep_alive()
        }
        return a.get(self.socket_type, self.close)

    def _send(self, message):
        if self.c is None:
            self.c = Client(self.port, self.hostname)
        self.c.send(message)

    def _recv(self):
        return self.c.recv()

    def _iot_message(self):
        return msgpack.packb(self.to_encrypt)

    def set_socket_type(self, _type):
        def switch(_type):
            return {
                "keep-alive"  : '0',
                "one-shot"    : '1',
                "full-duplex" : '2',
                "echo"        : '3'
            }.get(_type, '0')

        self.socket_type = switch(_type)
        self.to_encrypt[self._socket_idx] = self.socket_type


    def _set_pass(self):
        self.passphrase = Random.new().read(PASS_SIZE)
        self.to_encrypt[self._pass_idx] = self.passphrase

    def _set_iv(self):
        self.IV = Random.new().read(BLOCK_SIZE)
        self.to_encrypt[self._iv_idx] = self.IV

    def _set_microservicename(self, microservicename):
        _max_len = 16
        if len(microservicename.encode("utf-8")) <= _max_len:
            self.microservicename = microservicename.encode("utf-8")
            self.to_encrypt[self._msname_idx] = self.microservicename
        else:
            raise Exception("The Microsevice name, must be up to %s bytes" % (_max_len))

    def _fd_func(self):
        self.letter = self.response
        if self.empty == 5:
            self.keep_open = False
        self.empty += 1

    def _full_duplex(self):
        self.keep_open = True
        while self.keep_open:
            try:
                self._send(self.letter)
                self.response = self._recv()
                self._fd_func()
            except Exception as e:
                print(str(e))
                self.keep_open = False
        self.close()

    def _decrypt(self):
        aes = AES.new(self.passphrase, AES.MODE_CFB, self.IV)
        return aes.decrypt(self.response)

    def _encrypt(self, message):
        aes = AES.new(self.passphrase, AES.MODE_CFB, self.IV)
        return aes.encrypt(message)

    def _keep_alive(self):
        self._send(self.letter)

        self.response = self._recv()

        self.response = msgpack.unpackb(self._decrypt(), encoding = "utf-8")

        self.close()

    def _one_shot(self):
        self._send(self.letter)
        self.close()

    def set_key(self, rsa_key):
        self.rsa_key = rsa_key
        self.key =RSA.importKey(self.rsa_key)
        self.enc = PKCS1_OAEP.new(self.key)

    def get_rsa_key(self):
        self.letter = "{{KEYPEMCODE}}".encode("UTF-8")
        self._send(self.letter)
        rsa_key = self._recv()
        self.set_key(rsa_key)
        self.close()

    def close(self):
        self.c.close()
        self.c = None
