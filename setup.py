import setuptools

with open("readme.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="iotclient",
    version="0.0.1",
    author="Sergio Branco | The Architech",
    author_email="asergio.branco@gmail.com",
    description="The IoTClient package provides a way to connect any iotdevice to the cloud.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="iot.asergiobranco.com",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
        "Natural Language :: English",
        "Topic :: Communications"
    ],
)
